import React, { Component } from 'react';
//import logo from './logo.svg';
// import '../css/App.css';
import '../css/web.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="leftPanel">
          <div className="menuIcon"></div>
        </div>
        <div className="divPreview">
          <div className="divSubmit">
            Submit your work
          </div>
          <div className="divBody">
            <div className="mainIcon"></div>
            <div className="labelInspiration">
              Inspiration
            </div>
            <h2 className="mainHeading">Daily inspiration, right in your face.</h2>
            <div>
              <div class="tag">Tags:</div>
              <button class="button button1">Web Design</button>
              <button class="button button2">Branding</button>
              <button class="button button3">Illustration</button>
              <button class="button button4">Animation</button>
              <div class="button5">Select all</div>
              <div class="menu">.</div>

            </div>
            <br>
            </br>
            <div class="image">
              <div class="block">
                <div class="a">
                  <img class="img1" src="https://t4.ftcdn.net/jpg/02/13/60/21/160_F_213602148_LL3o8QZ654BylPRd6xFXgeSBgI8ai1yt.jpg" ></img>
                  <div class="p1">byPodis &emsp; &emsp; &emsp;&emsp;&nbsp;Dribble</div>
                </div>

                <div class="b"><img class="img2" src="https://welovedaily.uk/data/1884381287991299217_3940206495.jpg"></img>
                  <div class="p2">byDouglas Fuchs &emsp; &emsp; &emsp;Dribble</div>
                </div>
                <div class="c"><img class="img3" src="https://welovedaily.uk/data/1886434980341302034_3940206495.jpg"></img>
                  <div class="p3">byDouglas Fuchs&emsp; &emsp; Dribble</div>
                </div>
                <div class="d"><img class="img4" src="https://welovedaily.uk/data/1885711736419707242_3939283418.jpg"></img>
                  <div class="p4">byGiga Tamarashvili&emsp; Dribble</div>
                </div>
                <div class="e"><img class="img5" src="https://welovedaily.uk/data/1884985165602594824_3940206495.jpg"></img>
                  <div class="p5">byGiga Tamarashvili&emsp;Dribble</div>
                </div>
                <div class="f"><img class="img6" src="https://welovedaily.uk/data/1882931924551232753_3940206495.jpg"></img>
                  <div class="p6">byGiga Tamarashvili&emsp; Dribble</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br />
        <br />
        <br />
        <footer>
          <div className="foot"></div>
          <div className="foot1">
            <div className="a1"><h4>Welovedaily</h4></div>
            <div className="a2">Inspiration</div>
            <div className="a2">Challenges</div>
            <div className="a2">Adevertise</div>
            <div className="a2">About us</div>
          </div>
          <div className="foot2">
            <div className="a3"><h4>Docs.</h4></div>
            <div className="a4">Submitting work</div>
            <div className="a4">Joining a challenge</div>
            <div className="a4">Hosting a challenge</div>
          </div>
          <div className="foot3">
            <div className="a5"><h4>WebDesign<span class="d1">280K</span></h4></div>
            <div className="a6">Branding<span class="d2">242k</span></div>
            <div className="a6">Illustration<span class="d3">96.7K</span></div>
            <div className="a6">Animation<span class="d4">85.5K</span></div>

          </div>
          <div className="foot4">
            <div className="a7"><h4>Get In Touch</h4></div>
            <div className="a8"><h4>hello@welovedaily</h4></div>
            <div className="a9"><img class="f1" src="http://livecurious.ly/wp-content/uploads/2014/12/TC-Slide8.jpg"></img></div>
          </div>
        </footer>
      </div>


    );
  }
}

export default App;
